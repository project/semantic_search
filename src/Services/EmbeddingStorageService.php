<?php

namespace Drupal\semantic_search\Services;

use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Database\Connection;
use Drupal\Core\Entity\EntityTypeManager;
use Drupal\Core\File\FileSystem;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Logger\LoggerChannelFactory;
use Drupal\Core\Render\Renderer;
use Drupal\semantic_search\Http\PineconeClient;
use Pgvector\Vector;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Service to handle and store embeddings.
 *
 * @package Drupal\semantic_search\Services
 */
class EmbeddingStorageService {

  /**
   * The embeddings private directory.
   *
   * @var string
   */
  const EMBEDDING_PRIVATE_PATH = 'private://semantic_search/';

  /**
   * The file system service.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected $fileSystem;

  /**
   * The database connection service.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * The Pinecone client.
   *
   * @var \Drupal\semantic_search\Http\PineconeClient
   */
  protected $pinecone;

  /**
   * The Language Manager.
   *
   * @var \Drupal\Core\Config\ConfigFactory
   */
  protected ConfigFactory $configFactory;

  /**
   * The Logger Channel Factory.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactory
   */
  protected LoggerChannelFactory $loggerFactory;

  /**
   * The Embedding Vectorization service.
   *
   * @var \Drupal\semantic_search\Services\EmbeddingVectorizationService
   */
  protected EmbeddingVectorizationService $embeddingVectorizationService;

  /**
   * The Entity Type Manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManager
   */
  protected EntityTypeManager $entityTypeManager;

  /**
   * The Renderer service.
   *
   * @var \Drupal\Core\Render\Renderer
   */
  protected Renderer $renderer;

  /**
   * The Pinecone namespace.
   */
  const PINECONE_NAMESPACE = 'node:rendered_entity';

  /**
   * The Pinecone storage key.
   */
  const PINECONE_STORAGE_KEY = 'pinecone';

  /**
   * The local storage key.
   */
  const LOCAL_STORAGE_KEY = 'local';

  /**
   * The postgres storage key.
   */
  const POSTGRES_STORAGE_KEY = 'postgres';

  /**
   * Constructs for EmbeddingStorageService.
   *
   * @param \Drupal\Core\File\FileSystem $file_system
   *   The file system service.
   * @param \Drupal\semantic_search\Http\PineconeClient $pinecone_client
   *   The pinecone client.
   * @param \Drupal\Core\Database\Connection $connection
   *   The database connection.
   * @param \Drupal\Core\Config\ConfigFactory $config_factory
   *   The config factory.
   * @param \Drupal\Core\Logger\LoggerChannelFactory $logger_factory
   *   The logger factory.
   * @param \Drupal\semantic_search\Services\EmbeddingVectorizationService $embedding_vectorization_service
   *   The embedding vectorization service.
   * @param \Drupal\Core\Entity\EntityTypeManager $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Render\Renderer $renderer
   *   The renderer service.
   */
  public function __construct(
    FileSystem $file_system,
    PineconeClient $pinecone_client,
    Connection $connection,
    ConfigFactory $config_factory,
    LoggerChannelFactory $logger_factory,
    EmbeddingVectorizationService $embedding_vectorization_service,
    EntityTypeManager $entity_type_manager,
    Renderer $renderer) {
    $this->fileSystem = $file_system;
    $this->pinecone = $pinecone_client;
    $this->database = $connection;
    $this->configFactory = $config_factory;
    $this->loggerFactory = $logger_factory;
    $this->embeddingVectorizationService = $embedding_vectorization_service;
    $this->entityTypeManager = $entity_type_manager;
    $this->renderer = $renderer;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('file_system'),
      $container->get('semantic_search.pinecone_client'),
      $container->get('database'),
      $container->get('config.factory'),
      $container->get('logger.factory'),
      $container->get('semantic_search.embedding_vectorization'),
      $container->get('entity_type.manager'),
      $container->get('renderer')
    );
  }

  /**
   * Save embeddings to file.
   */
  public function saveEmbeddings($embeddings) {
    $path = self::EMBEDDING_PRIVATE_PATH;
    $this->fileSystem->prepareDirectory($path, FileSystemInterface::CREATE_DIRECTORY);

    $filepath = self::getLocalFilepath();
    $this->fileSystem->saveData(json_encode($embeddings, TRUE), $filepath, TRUE);
  }

  /**
   * Saves an embedding.
   *
   * @param resource $node
   *   The node.
   *
   * @return bool
   *   The result.
   */
  public function saveEmbedding($node) {

    $builder = $this->entityTypeManager->getViewBuilder('node');
    $build = $builder->view($node);
    $node_content = $this->renderer->render($build);

    $embedding = $this->embeddingVectorizationService->getEmbedding($node_content->jsonSerialize());

    if (!$embedding) {
      return FALSE;
    }

    switch ($this->configFactory->get('semantic_search.settings')->get('embedding_storage')) {
      case self::LOCAL_STORAGE_KEY:
        $saved_embeddings = $this->getSavedEmbeddings();
        $saved_embeddings[$node->id()] = $embedding;
        $this->saveEmbeddings($saved_embeddings);
        break;

      case $this::POSTGRES_STORAGE_KEY:
        $vector = new Vector($embedding);
        $table = $this->getPostgresTableName();
        $this->postgresRequest('INSERT INTO ' . $table . ' (embedding, content) VALUES ($1, $2)', [
          $vector,
          $node->id(),
        ]);
        break;

      case self::PINECONE_STORAGE_KEY:

        try {

          $id = 'entity:' . $node->id() . ':' . $node->getEntityTypeId() . ':' . $node->bundle();
          $vectors = [
            'id' => $id,
            'values' => $embedding,
            'metadata' => [
              'entity_id' => $node->id(),
              'entity_type' => $node->getEntityTypeId(),
              'bundle' => $node->bundle(),
            ],
          ];

          $this->pinecone->upsert($vectors, $this::PINECONE_NAMESPACE);

          $this->database->merge('semantic_search_embeddings')
            ->keys(
              [
                'entity_id' => $node->id(),
                'entity_type' => $node->getEntityTypeId(),
                'bundle' => $node->bundle(),
              ]
            )
            ->fields(
              [
                'embedding' => json_encode(['data' => $embeddings]) ?? [],
              ]
            )
            ->execute();

          sleep(1);
          usleep(200000);

        }
        catch (\Exception $e) {
          $this->loggerFactory->get('semantic_search')->error(
            'An exception occurred while trying to generate embeddings for a :entity_type with the ID of :entity_id. The bundle of this entity is :bundle. The error was :error',
            [
              ':entity_type' => $node->getEntityTypeId(),
              ':entity_id' => $node->id(),
              ':bundle' => $node->bundle(),
              ':error' => $e->getMessage(),
            ]
          );
        }
        break;
    }

    return TRUE;
  }

  /**
   * Removes an embedding.
   *
   * @param resource $node
   *   The node.
   */
  public function removeEmbedding($node) {
    $saved_embeddings = $this->getSavedEmbeddings();

    if (isset($saved_embeddings[$node->id()])) {
      unset($saved_embeddings[$node->id()]);
      $this->saveEmbeddings($saved_embeddings);
    }
  }

  /**
   * Get saved embeddings from file.
   *
   * @return array
   *   Saved embeddings.
   */
  public function getSavedEmbeddings() {

    $saved_embeddings = [];
    $filepath = $this->getLocalFilepath();
    if (file_exists($filepath)) {
      $embedding_data = file_get_contents($filepath);
      $saved_embeddings = json_decode($embedding_data, TRUE);
    }
    return $saved_embeddings;
  }

  /**
   * Returns the postgres connection.
   *
   * @return false|resource
   *   The connection.
   */
  public function getPostgresConnection() {
    $postgres_config = $this->configFactory->get('semantic_search.postgres_client');
    $db = pg_connect("host=" . $postgres_config->get('host') . " dbname=" . $postgres_config->get('db_name') . " user=" . $postgres_config->get('user') . " password=" . $postgres_config->get('password'));
    return $db;
  }

  /**
   * Performs a postgres request.
   *
   * @param string $query
   *   The query.
   * @param array $args
   *   The arguments.
   *
   * @return false|resource
   *   The result.
   */
  public function postgresRequest($query, array $args) {
    $db = $this->embeddingVectorizationService->getPostgresConnection();
    $result = pg_query_params($db, $query, $args);
    pg_close($db);
    return $result;
  }

  /**
   * Returns the Postgres tablename.
   *
   * @return string
   *   The tablename.
   */
  public function getPostgresTableName() {
    $vectorization_service = $this->embeddingVectorizationService->getVectorizationService();
    $vector_size = $this->embeddingVectorizationService->getVectorSize();
    $table_prefix = $this->configFactory->get('semantic_search.postgres_client')->get('table_prefix');
    return $table_prefix . '_' . $vectorization_service . '_' . $vector_size;
  }

  /**
   * Returns the local filename.
   *
   * @return string
   *   The filename.
   */
  public function getLocalFilename() {
    $vectorization_service = $this->embeddingVectorizationService->getVectorizationService();
    $vector_size = $this->embeddingVectorizationService->getVectorSize();
    return "semantic_search_vectors_" . $vectorization_service . '_' . $vector_size . ".json";
  }

  /**
   * Returns the local file path.
   *
   * @return string
   *   The path.
   */
  public function getLocalFilepath() {
    return self::EMBEDDING_PRIVATE_PATH . self::getLocalFilename();
  }

  /**
   * Delete the local storage file.
   */
  public function eraseLocalFile() {
    $filepath = $this->getLocalFilepath();
    $f = @fopen($filepath, "r+");
    if ($f !== FALSE) {
      ftruncate($f, 0);
      fclose($f);
    }
  }

}
