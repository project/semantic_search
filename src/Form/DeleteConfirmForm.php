<?php

namespace Drupal\semantic_search\Form;

use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Database\Connection;
use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\semantic_search\Http\PineconeClient;
use Drupal\semantic_search\Services\EmbeddingStorageService;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a confirm form to delete all Pinecone items.
 */
class DeleteConfirmForm extends ConfirmFormBase {

  /**
   * The database service.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * The Pinecone client.
   *
   * @var \Drupal\semantic_search\Http\PineconeClient
   */
  protected $client;

  /**
   * The Config Factory.
   *
   * @var \Drupal\Core\Config\ConfigFactory
   */
  protected $configFactory;

  /**
   * The Embedding Storage Service.
   *
   * @var \Drupal\semantic_search\Services\EmbeddingStorageService
   */
  protected $embeddingService;

  /**
   * Constructs a new DeleteConfirmForm object.
   *
   * @param \Drupal\Core\Database\Connection $database
   *   The database service.
   * @param \Drupal\semantic_search\Http\PineconeClient $client
   *   The Pinecone client.
   * @param \Drupal\Core\Config\ConfigFactory $config_factory
   *   The Config Factory.
   * @param \Drupal\semantic_search\Services\EmbeddingStorageService $embedding_service
   *   The Embedding Storage Service.
   */
  public function __construct(Connection $database, PineconeClient $client, ConfigFactory $config_factory, EmbeddingStorageService $embedding_service) {
    $this->database = $database;
    $this->client = $client;
    $this->configFactory = $config_factory;
    $this->embeddingService = $embedding_service;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('database'),
      $container->get('semantic_search.pinecone_client'),
      $container->get('config.factory'),
      $container->get('semantic_search.embedding_storage')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'semantic_search_embeddings_delete_confirm';
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return $this->t('Are you sure you want to delete all items in your %storage index?', ['%storage' => $this->configFactory->get('semantic_search.settings')->get('embedding_storage')]);
  }

  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    return $this->t('This will delete all items in your %storage index.', ['%storage' => $this->configFactory->get('semantic_search.settings')->get('embedding_storage')]);
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return Url::fromRoute('semantic_search.settings_form');
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    switch ($this->configFactory->get('semantic_search.settings')->get('embedding_storage')) {
      case EmbeddingStorageService::LOCAL_STORAGE_KEY:
        $this->embeddingService->eraseLocalFile();
        break;

      case EmbeddingStorageService::PINECONE_STORAGE_KEY:
        $this->client->delete([], TRUE, EmbeddingStorageService::PINECONE_NAMESPACE);

        $this->messenger()->addStatus($this->t('All items have been deleted in Pinecone.'));
        $form_state->setRedirect('semantic_search.settings_form');
        break;
    }

  }

}
