<?php

namespace Drupal\semantic_search\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\node\NodeInterface;
use Drupal\semantic_search\Services\EmbeddingStorageService;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Indexing form.
 */
class IndexingForm extends ConfigFormBase {

  /**
   * The EmbeddingStorageService service.
   *
   * @var \Drupal\semantic_search\Services\EmbeddingStorageService
   */
  protected $embeddingStorage;

  /**
   * Entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'semantic_search.indexing',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'semantic_search_form';
  }

  /**
   * Constructs a new IndexingForm object.
   *
   * @param \Drupal\semantic_search\Services\EmbeddingStorageService $embedding_storage
   *   The EmbeddingStorageService service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The Entity type manager.
   */
  public function __construct(EmbeddingStorageService $embedding_storage, $entity_type_manager) {
    $this->embeddingStorage = $embedding_storage;
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('semantic_search.embedding_storage'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $saved_embeddings = ($this->embeddingStorage->getSavedEmbeddings()) ? $this->embeddingStorage->getSavedEmbeddings() : [];

    $form['help'] = [
      '#type' => 'item',
      '#title' => $this->t('Indexed status'),
      '#markup' => $this->t('@indexed nodes', ['@indexed' => count($saved_embeddings)]),
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Index site'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $types = $this->config('semantic_search.entities')->get('entity_types');

    $query = $this->entityTypeManager->getStorage('node')->getQuery();

    $query
      ->accessCheck(TRUE)
      ->condition('status', NodeInterface::PUBLISHED)
      ->condition('type', $types, 'IN');
    $nodeIds = $query->execute();
    $nodes = $this->entityTypeManager->getStorage('node')->loadMultiple($nodeIds);

    $counter = 0;
    $batch = [
      'operations' => [],
      'title' => $this->t('Indexing Nodes'),
      'finished' => 'IndexingForm::indexingFinished',
    ];

    foreach ($nodes as $node) {
      $batch['operations'][] = [
        [$this, 'bulkIndexing'],
        [$node->id()],
      ];
      $counter++;
    }

    if ($counter > 0) {
      batch_set($batch);
      $this->messenger()->addStatus($this->t('Indexing launched for @counter Nodes.', ['@counter' => $counter]));
    }
    else {
      $this->messenger()->addWarning($this->t('Nothing to index.'));
    }

  }

  /**
   * Export the quotation.
   */
  public function bulkIndexing($nid, &$context) {

    $node = $this->entityTypeManager->getStorage('node')->load($nid);

    if ($this->embeddingStorage->saveEmbedding($node)) {
      $context['results']['count']++;
    }

  }

  /**
   * Batch Finished callback.
   *
   * @param bool $success
   *   Success of the operation.
   * @param array $results
   *   Array of results for post processing.
   * @param array $operations
   *   Array of operations.
   */
  public static function indexingFinished($success, array $results, array $operations) {
    if ($success) {

      $message = t('@exported_text', [
        '@exported_text' => t('@count Nodes Indexed', ['@count' => $results['count']]),
      ]);
      \Drupal::messenger()->addStatus($message);
    }
    else {
      $message = t('An error has occurred. Please contact an administrator.');
      \Drupal::messenger()->addError($message);
    }

  }

  /**
   * {@inheritdoc}
   */
  protected function cleanRenderedField($content) {
    $content = strip_tags($content);
    $content = preg_replace('~[\r\n]+~', '', $content);
    return $content;
  }

}
