<?php

namespace Drupal\semantic_search\Form;

use Drupal\Core\Entity\EntityTypeBundleInfo;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\semantic_search\Services\EmbeddingStorageService;
use Drupal\semantic_search\Services\EmbeddingVectorizationService;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Semantic Search configuration form.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * The EntityTypeBundleInfo service.
   *
   * @var \Drupal\Core\Entity\EntityTypeBundleInfo
   */
  protected $bundleInfo;

  /**
   * The EmbeddingVectorizationService service.
   *
   * @var \Drupal\semantic_search\Services\EmbeddingVectorizationService
   */
  protected EmbeddingVectorizationService $embeddingVectorizationService;

  /**
   * The EmbeddingStorageService service.
   *
   * @var \Drupal\semantic_search\Services\EmbeddingStorageService
   */
  protected EmbeddingStorageService $embeddingStorageService;

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'semantic_search.settings',
      'semantic_search.openai_client',
      'semantic_search.pinecone_client',
      'semantic_search.postgres_client',
      'semantic_search.custom_vectorization_api',
      'semantic_search.entities',
    ];
  }

  /**
   * Constructs a new SettingsForm object.
   *
   * @param \Drupal\Core\Entity\EntityTypeBundleInfo $bundle_info
   *   The EntityTypeBundleInfo service.
   * @param \Drupal\semantic_search\Services\EmbeddingVectorizationService $embedding_vectorization_service
   *   The EmbeddingVectorizationService service.
   * @param \Drupal\semantic_search\Services\EmbeddingStorageService $embedding_storage_service
   *   The EmbeddingStorageService service.
   */
  public function __construct(EntityTypeBundleInfo $bundle_info, EmbeddingVectorizationService $embedding_vectorization_service, EmbeddingStorageService $embedding_storage_service) {
    $this->bundleInfo = $bundle_info;
    $this->embeddingVectorizationService = $embedding_vectorization_service;
    $this->embeddingStorageService = $embedding_storage_service;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.bundle.info'),
      $container->get('semantic_search.embedding_vectorization'),
      $container->get('semantic_search.embedding_storage')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'semantic_search_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $global_config = $this->config('semantic_search.settings');

    foreach ($this->embeddingVectorizationService::STORAGE_OPTIONS as $option) {
      $embedding_vectorization_options[$option['key']] = $option['label'];
    }

    $form['embedding_vectorization'] = [
      '#type' => 'select',
      '#options' => $embedding_vectorization_options,
      '#title' => $this->t('Embedding Vectorization'),
      '#default_value' => $global_config->get('embedding_vectorization'),
      '#required' => TRUE,
    ];

    $form['vectorization'] = [
      '#type' => 'fieldset',
      '#tree' => TRUE,
      '#title' => $this->t('Configure API clients for vectorization'),
      '#description' => $this->t('Searching vector/embedding data is only available one of these services.... TBD'),
    ];

    $form['vectorization']['openai'] = [
      '#type' => 'details',
      '#title' => $this->t('Open AI'),
      '#description' => $this->t('Configure Open AI settings'),
    ];

    $openai_config = $this->config('semantic_search.openai_client');

    $form['vectorization']['openai']['api_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('OpenAI Key'),
      '#description' => $this->t('OpenAI Key.'),
      '#default_value' => $openai_config->get('openai_key'),
      '#required' => TRUE,
    ];

    $form['vectorization']['custom_vectorization_api'] = [
      '#type' => 'details',
      '#title' => $this->t('Custom Vectorization API'),
      '#description' => $this->t('Configure Custom Vectorization API settings'),
    ];

    $custom_vectorization_api_config = $this->config('semantic_search.custom_vectorization_api');

    $form['vectorization']['custom_vectorization_api']['url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('URL'),
      '#default_value' => $custom_vectorization_api_config->get('url'),
      '#required' => TRUE,
    ];

    $form['vectorization']['custom_vectorization_api']['auth_token'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Auth token'),
      '#default_value' => $custom_vectorization_api_config->get('auth_token'),
      '#required' => TRUE,
    ];

    $embedding_storage_options = [
      EmbeddingStorageService::PINECONE_STORAGE_KEY => 'Pinecone',
      EmbeddingStorageService::LOCAL_STORAGE_KEY => 'Local Storage',
      EmbeddingStorageService::POSTGRES_STORAGE_KEY => 'Postgres',
    ];

    $form['embedding_storage'] = [
      '#type' => 'select',
      '#options' => $embedding_storage_options,
      '#title' => $this->t('Embedding Storage'),
      '#default_value' => $global_config->get('embedding_storage'),
      '#required' => TRUE,
    ];

    $similarity_options = [];
    foreach (range(99, 70, 1) as $number) {
      $similarity_options['0.' . $number] = '0.' . $number;
    }

    $form['similarity_min'] = [
      '#type' => 'select',
      '#options' => $similarity_options,
      '#title' => $this->t('Similarity Minimum'),
      '#description' => $this->t('Similarity Minimum'),
      '#default_value' => $global_config->get('similarity_min'),
      '#required' => TRUE,
    ];

    $form['connections'] = [
      '#type' => 'fieldset',
      '#tree' => TRUE,
      '#title' => $this->t('Configure API clients for vector search database services'),
      '#description' => $this->t('Searching vector/embedding data is only available one of these services.... TBD'),
    ];

    $form['connections']['pinecone'] = [
      '#type' => 'details',
      '#title' => $this->t('Pinecone'),
      '#description' => $this->t('Configure Pinecone settings (need links + description)'),
    ];

    $pinecone_config = $this->config('semantic_search.pinecone_client');

    $form['connections']['pinecone']['api_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('API Key'),
      '#default_value' => $pinecone_config->get('api_key'),
      '#description' => $this->t('The API key is required to make calls to Pinecone for vector searching.'),
    ];

    $form['connections']['pinecone']['hostname'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Hostname'),
      '#default_value' => $pinecone_config->get('hostname'),
      '#description' => $this->t('The hostname or base URI where your Pinecone instance is located.'),
    ];

    $form['connections']['postgres'] = [
      '#type' => 'details',
      '#title' => $this->t('Postgres'),
      '#description' => $this->t('Configure Postgres settings (need links + description)'),
    ];

    $form['connections']['postgres']['host'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Host'),
      '#default_value' => $this->config('semantic_search.postgres_client')->get('host'),
    ];

    $form['connections']['postgres']['db_name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('DB Name'),
      '#default_value' => $this->config('semantic_search.postgres_client')->get('db_name'),
    ];

    $form['connections']['postgres']['user'] = [
      '#type' => 'textfield',
      '#title' => $this->t('User'),
      '#default_value' => $this->config('semantic_search.postgres_client')->get('user'),
    ];

    $form['connections']['postgres']['password'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Password'),
      '#default_value' => $this->config('semantic_search.postgres_client')->get('password'),
    ];

    $form['connections']['postgres']['table_prefix'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Table Prefix'),
      '#default_value' => $this->config('semantic_search.postgres_client')->get('table_prefix'),
      '#description' => $this->t('The table name will be made of the prefix + the vectorizing service.'),
    ];

    $form['entities'] = [
      '#type' => 'fieldset',
      '#tree' => TRUE,
      '#title' => $this->t('Configure which entity types should be indexed'),
    ];

    $types = $this->bundleInfo->getBundleInfo('node');
    $options = [];
    foreach ($types as $key => $type) {
      $options[$key] = $type['label'];
    }

    $default_value = $this->config('semantic_search.entities')->get('entity_types') ? $this->config('semantic_search.entities')->get('entity_types') : [];

    $form['entities']['entity_types'] = [
      '#type' => 'checkboxes',
      '#options' => $options,
      '#title' => $this->t('Node types'),
      '#default_value' => $default_value,
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $this->config('semantic_search.settings')
      ->set('similarity_min', trim($form_state->getValue('similarity_min')))
      ->set('embedding_storage', trim($form_state->getValue('embedding_storage')))
      ->set('embedding_vectorization', trim($form_state->getValue('embedding_vectorization')))
      ->save();

    $this->config('semantic_search.openai_client')
      ->set('openai_key', trim($form_state->getValue('vectorization')['openai']['api_key']))
      ->save();

    $this->config('semantic_search.custom_vectorization_api')
      ->set('url', trim($form_state->getValue('vectorization')['custom_vectorization_api']['url']))
      ->set('auth_token', trim($form_state->getValue('vectorization')['custom_vectorization_api']['auth_token']))
      ->save();

    $pinecone = $form_state->getValue('connections')['pinecone'];

    $this->config('semantic_search.pinecone_client')
      ->set('api_key', $pinecone['api_key'])
      ->set('hostname', $pinecone['hostname'])
      ->save();

    $postgres = $form_state->getValue('connections')['postgres'];

    $this->config('semantic_search.postgres_client')
      ->set('host', $postgres['host'])
      ->set('db_name', $postgres['db_name'])
      ->set('user', $postgres['user'])
      ->set('password', $postgres['password'])
      ->set('table_prefix', $postgres['table_prefix'])
      ->save();

    $this->config('semantic_search.entities')
      ->set('entity_types', $form_state->getValue('entities')['entity_types'])
      ->save();

    // Check if Postgres just got enabled, or vectorization service changed,
    // so we create the table.
    $postgres_on = ($form_state->getValue('embedding_storage') == EmbeddingStorageService::POSTGRES_STORAGE_KEY);
    $postgres_just_on = ($form_state->getValue('embedding_storage') <> $form['embedding_storage']['#default_value'] && $postgres_on);
    $vectorization_change = ($form_state->getValue('embedding_vectorization') <> $form['embedding_vectorization']['#default_value']);

    if ($postgres_just_on || ($vectorization_change && $postgres_on)) {

      $table_name = $this->embeddingStorageService->getPostgresTableName();
      $db = $this->embeddingStorageService->getPostgresConnection();
      $size = $this->embeddingVectorizationService->getVectorSize();

      pg_query($db, 'CREATE EXTENSION IF NOT EXISTS vector');
      pg_query($db, 'DROP TABLE IF EXISTS ' . $table_name);
      pg_query($db, 'CREATE TABLE ' . $table_name . ' (id bigserial primary key, content text, embedding vector(' . $size . '))');
    }

  }

}
