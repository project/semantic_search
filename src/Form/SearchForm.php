<?php

namespace Drupal\semantic_search\Form;

use Algenza\Cosinesimilarity\Cosine;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Language\LanguageManager;
use Drupal\semantic_search\Services\EmbeddingStorageService;
use Drupal\semantic_search\Services\EmbeddingVectorizationService;
use Pgvector\Vector;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * SearchForm form class.
 */
class SearchForm extends FormBase {

  /**
   * The Embedding Vectorization client.
   *
   * @var \Drupal\semantic_search\Services\EmbeddingVectorizationService
   */
  protected $vectorizingClient;

  /**
   * The Pinecone HTTP client.
   *
   * @var \Drupal\semantic_search\Http\PineconeClient
   */
  protected $pinecone;

  /**
   * The Entity Type Manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManager
   */
  protected $entityTypeManager;

  /**
   * The Language Manager.
   *
   * @var \Drupal\Core\Language\LanguageManager
   */
  protected LanguageManager $languageManager;

  /**
   * The Config Factory.
   *
   * @var \Drupal\Core\Config\ConfigFactory
   */
  protected $configFactory;


  /**
   * The Embedding Storage service.
   *
   * @var \Drupal\semantic_search\Services\EmbeddingStorageService
   */
  protected EmbeddingStorageService $embeddingStorage;

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'semantic_search_form';
  }

  /**
   * Constructs a new SearchForm object.
   *
   * @param \Drupal\semantic_search\Services\EmbeddingVectorizationService $embedding_vectorization
   *   The EmbeddingVectorizationService service.
   * @param \Drupal\semantic_search\Http\PineconeClient $pinecone
   *   The Pinecone HTTP client.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The Entity type manager.
   * @param \Drupal\Core\Language\LanguageManager $language_manager
   *   The Language Manager.
   * @param \Drupal\semantic_search\Services\EmbeddingStorageService $embedding_storage
   *   The Embedding Storage service.
   * @param \Drupal\Core\Config\ConfigFactory $config_factory
   *   The Config Factory.
   */
  public function __construct(EmbeddingVectorizationService $embedding_vectorization, $pinecone, $entity_type_manager, LanguageManager $language_manager, EmbeddingStorageService $embedding_storage, $config_factory) {
    $this->vectorizingClient = $embedding_vectorization;
    $this->pinecone = $pinecone;
    $this->entityTypeManager = $entity_type_manager;
    $this->languageManager = $language_manager;
    $this->embeddingStorage = $embedding_storage;
    $this->configFactory = $config_factory;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('semantic_search.embedding_vectorization'),
      $container->get('semantic_search.pinecone_client'),
      $container->get('entity_type.manager'),
      $container->get('language_manager'),
      $container->get('semantic_search.embedding_storage'),
      $container->get('config.factory')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form['semantic_search'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Search'),
      '#required' => TRUE,
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
    ];

    if (isset($form_state->getUserInput()['semantic_search']) && $query = $form_state->getUserInput()['semantic_search']) {

      $similarity_min = $this->configFactory->get('semantic_search.settings')->get('similarity_min');

      $search_embedding = $this->vectorizingClient->getEmbedding($query);

      $result_ids = [];

      switch ($this->configFactory->get('semantic_search.settings')->get('embedding_storage')) {
        case EmbeddingStorageService::LOCAL_STORAGE_KEY:

          $embeddings = $this->embeddingStorage->getSavedEmbeddings();

          $similarities = [];
          foreach ($embeddings as $key => $embedding) {
            $search_array = is_string($search_embedding) ? json_decode($search_embedding) : $search_embedding;
            $embedding_array = is_string($embedding) ? json_decode($embedding) : $embedding;
            $cosine_similarity = Cosine::similarity($search_array, $embedding_array);
            if ($cosine_similarity > floatval($similarity_min)) {
              $similarities[$key] = $cosine_similarity;
            }
          }

          if ($similarities) {

            arsort($similarities);

            foreach ($similarities as $key => $similarity) {
              $result_ids[$key] = $key;
            }

          }
          break;

        case EmbeddingStorageService::PINECONE_STORAGE_KEY:

          $result = $this->vectorizingClient->getEmbedding($query);

          $pinecone_query = $this->pinecone->query(
            $result,
            8,
            TRUE,
            FALSE,
            [],
            EmbeddingStorageService::PINECONE_NAMESPACE,
          );

          $result = json_decode($pinecone_query->getBody()->getContents());
          $tracked = [];

          foreach ($result->matches as $match) {
            if (isset($tracked[$match->metadata->entity_type]) && in_array($match->metadata->entity_id, $tracked[$match->metadata->entity_type])) {
              continue;
            }

            if ($match->score < $similarity_min) {
              continue;
            }

            $result_ids[] = $match->metadata->entity_id;

            $tracked[$match->metadata->entity_type][] = $match->metadata->entity_id;
          }

          if (empty($tracked)) {
            $output = '<p>No results were found, or results were excluded because they did not meet the relevancy score threshold.</p>';
          }

          $form['semantic_search_result'][] = ['#markup' => '<div>' . $output . '</div>'];
          break;

        case EmbeddingStorageService::POSTGRES_STORAGE_KEY:

          $output = "";
          $embedding = new Vector($search_embedding);

          $table_name = $this->embeddingStorage->getPostgresTableName();
          $result = $this->embeddingStorage->postgresRequest('SELECT * FROM ' . $table_name . ' ORDER BY embedding <-> $1 LIMIT 5', [$embedding]);

          if ($result) {
            while ($row = pg_fetch_array($result)) {
              $result_ids[] = $row['content'];
            }
          }
          break;
      }

      if (count($result_ids)) {

        $nodes = $this->entityTypeManager->getStorage('node')->loadMultiple($result_ids);
        foreach ($nodes as $node) {

          $current_lang = $this->languageManager->getCurrentLanguage()->getId();
          if ($node->hasTranslation($current_lang)) {
            $node = $node->getTranslation($current_lang);
          }

          $score_txt = isset($similarities) ? ' (' . $similarities[$node->id()] . ')' : '';

          $form['semantic_search_result'][] = ['#markup' => '<div><a href="' . $node->toUrl()->toString() . '">' . $node->getTitle() . $score_txt . '</a></div>'];
        }
      }
      else {
        $form['semantic_search_result'][] = ['#markup' => '<div>' . $this->t('No match found') . '</div>'];

      }

    }
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $form_state->setRebuild(TRUE);

  }

}
