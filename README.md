# Semantic Search

The Drupal Semantic Search module makes it possible to enhance your
Search engine using AI tools.

The aim of this module is to integrate with various AI libraries to leverage AI-powered search functionnalities. While the [OpenAI API module](https://openai.com/) already offers semantic search faculties thanks to its embedding submodule, it is tightly ties (as its name suggests) to the OpenAI ecosystem. This is rather limiting and rises GDPR and privacy issues, since all data is being sent to a private company. We solve this limitation by opening the vectorization and embedding storage options to other vendors.

Currently  what  it does is :
- Select   which  node  types  you want to index
- Select which   vectorization  API you  want to use to calculate your vectors
- Select which storage engine you want to store your vectors in

For a full description of the module, visit the
[project page](http://drupal.org/project/semantic_search).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](http://drupal.org/project/issues/semantic_search).

## Table of contents

- Requirements
- Installation
- Webserver Streaming Support
- Planned Functionality
- Maintainers

## Requirements

This module is tested on Drupal 10.x.

## Installation

Enable the Semantic Search module and one or more submodules that meet your
need.

### Vectorization

We currently offer two options for generating embeddings.

#### OpenAI

You need to provide an OpenAI key.

#### Custom API

You need to provide the URL and Authorization Token to your custom model.

### Embedding Storage

3 options are now available.

#### Local Storage

Objects are stored locally in a JSON file. No requirements here.

#### Postgres Server

You need access to a Postgres server (version 13+) with the [Vector extension](https://supabase.com/docs/guides/database/extensions/pgvector) enabled.

#### Pinecone

You need to create an index, with a vector size equal to your
vectorization requirements (e.g. OpenAI is 1536).

## Planned functionality

This module is under active development, and all suggestions / contributions
are welcomed.
In the meantime, we plan the following developments :
- Search API Integration
- More connectors to models
- More connectors to vector storage options
- Content moderation
- Views integration
- and more

## Maintainers

- Guillaume Cambon - [oeklesund](https://www.drupal.org/u/oeklesund)
- Alain Gabelle - [AlGab](https://www.drupal.org/u/algab)
